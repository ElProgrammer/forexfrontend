angular.module('forex', ['ui.router', 'ngStorage', 'ui.bootstrap', 'ngSanitize', 'ngToast', 'angular.filter',
    'ngProgress','ngAnimate','textAngular'])
    .config(['$urlRouterProvider', '$stateProvider',

        function ($urlRouterProvider, $stateProvider) {
            $stateProviderRef = $stateProvider;
            $urlRouterProvider.otherwise('/');
            $stateProvider.state("main", {
                url: "/",
                templateUrl: 'main.html',
                controller: 'main.controller'
            }).state('temas', {
                url: "/temas/{foro}",
                controller: 'temas.controller',
                params: {
                    foro: null
                },
                templateUrl: 'temas.html',
                onEnter: function ($state, $stateParams) {
                    if ($stateParams.foro === null) {
                        $state.go("main")
                    }
                }

            }).state("mensaje", {
                url: "/{foro}/{idTema}/{tema}",
                params: {
                    foro: null,
                    idTema: null,
                    tema: null
                },
                templateUrl: 'mensajes.html',
                controller: 'mensaje.controller'
            });

        }]);