angular
    .module('forex')
    .controller('main.controller',
        ['foro.service', '$scope', 'ngProgressFactory',
            function (foroService, $scope, ngProgressFactory) {
                $scope.ngProgress = ngProgressFactory.createInstance();
                $scope.ngProgress.setColor('#25ff15');
                $scope.ngProgress.start();
                foroService.getForos().then
                (function success(result) {
                    $scope.foros = result.data;
                    $scope.ngProgress.complete();
                }, function error(result) {

                });
            }]);
