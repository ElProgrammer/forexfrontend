angular
    .module('forex')
    .controller('mensaje.controller',
        ['mensaje.service', '$scope', 'ngProgressFactory', '$stateParams', '$sessionStorage','ngToast',
            function (mensajeService, $scope, ngProgressFactory, $stateParams, $sessionStorage,ngToast) {
                $scope.storage= $sessionStorage;
                $scope.mostrar = false;
                $scope.error = false;
                $scope.contenido = "";
                $scope.idTema = $stateParams.idTema;
                $scope.foro = $stateParams.foro;
                $scope.tema = $stateParams.tema;
                $scope.cantidad = 25;
                $scope.ngProgress = ngProgressFactory.createInstance();
                $scope.ngProgress.setColor('#25ff15');
                $scope.ngProgress.start();
                mensajeService.getMensajes($scope.idTema, 1, $scope.cantidad).then
                (function success(result) {
                    if (result.data.data.length === 0) {
                        $scope.error = true;
                    } else {
                        $scope.pagina = result.data.pagina;
                        $scope.cantidad = result.data.cantElement;
                        $scope.maxPaginas = result.data.maxPaginas;
                        $scope.mensajes = result.data.data;
                        $scope.mostrar = true;
                    }
                    // $scope.mostrar = true;
                    $scope.ngProgress.complete();
                }, function error(result) {
                    console.log("error");

                });

                $scope.setPagina = function (pagina) {

                    $scope.ngProgress = ngProgressFactory.createInstance();
                    $scope.ngProgress.setColor('#25ff15');
                    $scope.ngProgress.start();
                    mensajeService.getMensajes($scope.idTema, 1, $scope.cantidad).then
                    (function success(result) {
                        $scope.pagina = result.data.pagina;
                        $scope.cantElement = result.data.cantElement;
                        $scope.maxPaginas = result.data.maxPaginas;
                        $scope.mensajes = result.data.data;
                        $scope.generarPaginas();
                        $scope.ngProgress.complete();
                    }, function error(result) {

                    });
                };

                $scope.generarPaginas = function () {
                    var min = ((parseInt($scope.pagina) - 5) <= 0 ? 1 : (parseInt($scope.pagina) - 5));
                    var max = ((parseInt($scope.pagina) + 5) > $scope.maxPaginas ? $scope.maxPaginas : (parseInt($scope.pagina) + 5));
                    var input = [];
                    for (var i = min; i <= max; i++) {
                        input.push(i);
                    }
                    return input;

                };

                $scope.enviarMensaje = function () {
                    mensajeService.setMensaje($scope.contenido.replace(/[\u200B]/g, ''),$scope.idTema,$scope.storage.nick).then(
                        function (success) {
                            console.log(success);
                            if (success.data.success === false) {
                                ngToast.create({
                                    className: 'warning',
                                    content: success.data.mensaje,
                                    animation: "fade"
                                });
                            } else {
                                ngToast.create(success.data.mensaje);
                                $scope.setPagina($scope.maxPaginas);
                                $scope.mensaje = "";
                            }
                        }, function (error) {

                        });
                }

            }]);