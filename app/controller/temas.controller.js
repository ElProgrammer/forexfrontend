angular
    .module('forex')
    .controller('temas.controller',
        ['tema.service', '$scope', 'ngProgressFactory', '$stateParams', '$compile',
            function (temaService, $scope, ngProgressFactory, $stateParams, $compile) {
                $scope.mostrar = false;
                $scope.error = false;
                $scope.service = temaService;
                $scope.foro = $stateParams.foro;
                $scope.cantidad = 25;
                $scope.ngProgress = ngProgressFactory.createInstance();
                $scope.ngProgress.setColor('#25ff15');
                $scope.ngProgress.start();
                temaService.getTemas($scope.foro, 1, $scope.cantidad).then
                (function success(result) {
                    if (result.data.data.length === 0) {
                        $scope.error = true;
                    } else {
                        $scope.pagina = result.data.pagina;
                        $scope.cantidad = result.data.cantElement;
                        $scope.maxPaginas = result.data.maxPaginas;
                        $scope.temas = result.data.data;
                        $scope.mostrar = true;
                    }
                   // $scope.mostrar = true;
                    $scope.ngProgress.complete();
                }, function error(result) {
                    console.log("error");

                });
            }]);