angular
    .module('forex')
    .controller('registrarse.controller',
        ['$scope', 'usuario.service', 'ngToast', '$uibModalInstance',
            function ($scope, usuarioService, ngToast, $uibModalInstance) {
                $scope.usuario = "";
                $scope.nombre = "";
                $scope.apellido = "";
                $scope.email = "";
                $scope.pass = "";

                $scope.registrarse = function () {
                    usuarioService.registrarse($scope.usuario, $scope.nombre,$scope.apellido,$scope.email,$scope.pass)
                        .then(
                            function (success) {
                                if (success.data.success === false) {
                                    ngToast.create({
                                        className: 'warning',
                                        content: success.data.mensaje,
                                        animation: "fade"
                                    });
                                } else {
                                    ngToast.create(success.data.mensaje);
                                    $scope.ok(success.data.data);
                                }
                            }, function (error) {

                            })
                };

                $scope.ok = function (data) {
                    $uibModalInstance.close(data);
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };


            }]);