angular
    .module('forex')
    .controller('nuevoTema.controller',
        ['$scope', 'tema.service', 'foro.service', 'ngToast', '$uibModalInstance', '$sessionStorage','$sce',
            function ($scope, temaService, foroService, ngToast, $uibModalInstance, $sessionStorage,$sce) {
                $scope.storage = $sessionStorage;
                $scope.titulo = "";
                $scope.contenido = "";
                foroService.getForos().then(function (success) {
                    $scope.foros = success.data;
                    $scope.foro = success.data[0];
                });

                $scope.crearTema = function () {
                    temaService.setTema($scope.titulo, $scope.contenido.replace(/[\u200B]/g, ''), $scope.foro.id, $scope.storage.nick)
                        .then(
                            function (success) {
                                console.log(success);
                                if (success.data.success === false) {
                                    ngToast.create({
                                        className: 'warning',
                                        content: success.data.mensaje,
                                        animation: "fade"
                                    });
                                } else {
                                    ngToast.create(success.data.mensaje);
                                    $scope.ok($scope.foro.nombre);
                                    $scope.cancel();
                                }
                            }, function (error) {

                            })
                };

                $scope.ok = function (nombre) {
                    $uibModalInstance.close(nombre);
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };


            }]);