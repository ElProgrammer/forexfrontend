angular
    .module('forex')
    .controller('index.controller',
        ['$scope', '$uibModal', '$sessionStorage', 'ngToast', 'usuario.service', '$state',
            function ($scope, $uibModal, $sessionStorage, ngToast, usuarioService, $state) {
                $scope.text = [];
                $scope.text.login = "Iniciar Sesion";
                $scope.text.register = "Registrarse";
                $scope.text.nuevoTema = '<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Nuevo tema';
                $scope.storage = $sessionStorage;
                init();
                $scope.iniciarSesion = function () {
                    modalInstance = $uibModal.open({
                        templateUrl: '../template/iniciarSesion.html',
                        controller: 'sesion.controller',
                        size: "md"

                    });

                    modalInstance.result.then(function (data) {
                        $scope.storage = $sessionStorage;
                        $scope.storage.nick = data.nick;
                    }, function () {

                    });

                };

                $scope.registrarse = function () {
                    modalInstance = $uibModal.open({
                        templateUrl: '../template/registrarse.html',
                        controller: 'registrarse.controller',
                        size: "md"

                    });

                    modalInstance.result.then(function (data) {

                    }, function () {

                    });

                };

                $scope.nuevoTema = function () {
                    modalInstance = $uibModal.open({
                        templateUrl: '../template/nuevoTema.html',
                        controller: 'nuevoTema.controller',
                        size: "lg"

                    });

                    modalInstance.result.then(function (nombre) {
                        $state.go('temas', {foro: nombre});
                    }, function () {

                    });

                };

                $scope.cerrarSesion = function () {
                    usuarioService.cerrarSesion().then(function (success) {
                        if (success.data.success === true) {
                            $sessionStorage.$reset();
                            ngToast.create(success.data.mensaje)
                        }
                    })
                };

                function init() {
                    if (typeof $scope.storage.nick === 'undefined' || $scope.storage.nick === null) {
                        $scope.storage.nick = null;
                    }
                }
            }]);