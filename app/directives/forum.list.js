angular.module('forex')
    .directive('forumList', function () {
        return {
            restrict: 'E',
            scope: {
                foros: '='
            },
            template: '<div ng-repeat="(key, value) in foros | groupBy: \'categoria\' " class="panel panel-primary">' +
            '<div class="panel-heading">{{key}}</div>' +
            '<div class="panel-body">' +
            '<div class="panel-body">' +
            '<div ng-repeat="foro in value track by foro.id" class="panel panel-default borderless no-margin">' +
            '<div class="panel-body">' +
            '<a href="" ng-click="goForum(foro.nombre)">{{foro.nombre}}</a> <br>' +
            '<h5>Mensajes: {{foro.cantidadMensajes}} Temas: {{foro.cantidadTemas}}</h5>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>', controller: ['$scope', '$state', function ($scope, $state) {

                $scope.goForum = function (nombre) {
                    $state.go('temas', {foro: nombre});
                };
            }]
        }

    });


angular.module('forex')
    .directive('topicsList', function () {
        return {
            restrict: 'E',
            scope: {
                temas: '=',
                pagina: '=',
                maxPaginas: '=',
                cantElement: '=',
                foro: '=',
                service: '='
            },
            template: '<div class="panel panel-primary">' +
            '<div class="panel-heading">' +
            '<ul id="pag" class="no-margin pagination pagination-sm">' +
            '<li ng-class="{\'disabled\': pagina==1 }" ng-click="pagina==1 || setPagina(1)">' +
            '<a class="animation" href="">&laquo;</a></li>' +
            '<li ng-repeat="n in generarPaginas()" ng-class="{\'disabled\': pagina==n }">' +
            '<a class="animation" ng-click="pagina==n || setPagina(n)" href="">{{n}}</a>' +
            '</li>' +
            '<li ng-class="{\'disabled\': pagina==maxPaginas }" ng-click="pagina==maxPaginas || setPagina(maxPaginas)">' +
            '<a class="animation" href="">&raquo;</a></li>' +
            '</ul>' +
            '</div>' +
            '<div class="panel-body">' +
            '<div class="panel-body">' +
            '<div ng-repeat="tema in temas" class="panel panel-default borderless no-margin">' +
            '<div class="panel-body">' +
            '<div class="row">' +
            '<div class="col-md-6">' +
            '<a ng-click="goTema(tema.idtema,tema.nombreTema,foro)" href="">{{tema.nombreTema}}</a>' +
            '<br>' +
            '<label class="font-size-12"><a class="text-warning" href="">{{tema.nickUsuario}}</a>, {{tema.fechaTema}}</label>' +
            '</div>' +
            '<div class="col-xs-6 col-md-offset-1 col-md-2"><p>Respuestas: {{tema.respuestas}}</p></div>' +
            '<div class="col-xs-6 col-md-3"><p align="right">Ultimo mensaje: {{tema.nickUltimoMensaje}} <br>' +
            '<span class="font-size-12">{{tema.fechaUltimoMensaje}}</span></p></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="panel-heading">' +
            '<ul class="no-margin pagination pagination-sm">' +
            '<li ng-class="{\'disabled\': pagina==1 }">' +
            '<a class="animation" ng-click="pagina==1 || setPagina(1)" href="">&laquo;</a></li>' +
            '<li ng-repeat="n in generarPaginas()" ng-class="{\'disabled\': pagina==n }">' +
            '<a class="animation" ng-click="pagina==n || setPagina(n)" href="">{{n}}</a>' +
            '<li ng-class="{\'disabled\': pagina==maxPaginas }">' +
            '<a class="animation" ng-click="pagina==maxPaginas || setPagina(maxPaginas)" href="">&raquo;</a></li>' +
            '</ul>' +
            '</div>' +
            '</div>',
            controller: ['$scope', 'ngProgressFactory', '$state', function ($scope, ngProgressFactory, $state) {

                $scope.setPagina = function (pagina) {

                    $scope.ngProgress = ngProgressFactory.createInstance();
                    $scope.ngProgress.setColor('#25ff15');
                    $scope.ngProgress.start();
                    $scope.service.getTemas($scope.foro, pagina, $scope.cantElement).then
                    (function success(result) {
                        $scope.pagina = result.data.pagina;
                        $scope.cantElement = result.data.cantElement;
                        $scope.maxPaginas = result.data.maxPaginas;
                        $scope.temas = result.data.data;
                        $scope.generarPaginas();
                        $scope.ngProgress.complete();
                    }, function error(result) {

                    });
                };

                $scope.goTema = function (idTema, tema, foro) {
                    console.log(foro);
                    $state.go('mensaje', {
                        foro: foro,
                        idTema: idTema,
                        tema: tema
                    });
                };


                $scope.generarPaginas = function () {
                    var min = ((parseInt($scope.pagina) - 5) <= 0 ? 1 : (parseInt($scope.pagina) - 5));
                    var max = ((parseInt($scope.pagina) + 5) > $scope.maxPaginas ? $scope.maxPaginas : (parseInt($scope.pagina) + 5));
                    var input = [];
                    for (var i = min; i <= max; i++) {
                        input.push(i);
                    }
                    return input;

                };

            }]
        }

    });


angular.module('forex')
    .directive('error', function () {
        return {
            restrict: 'E',
            scope: {
                header: '=',
                mensaje: '='
            },
            template: '<div class="alert alert-dismissible alert-danger">' +
            '<h4>{{header}}</h4>' +
            '<p>{{mensaje}}</p>' +
            '</div>'

        }

    });