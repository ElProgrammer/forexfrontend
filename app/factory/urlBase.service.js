angular
    .module('forex')
    .factory("urlBase.service", function () {
        var urlBase = 'http://localhost:80/forEXApi/index.php/api';
        var factory = {};

        factory.getUrlBase = function (pagina,cantidad) {
            return urlBase;
        };

        return factory;
    });