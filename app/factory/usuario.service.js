angular
    .module('forex')
    .factory("usuario.service", function ($http) {
        var urlBase = 'http://localhost:80/forEXApi/index.php/api';
        var factory = {};

        factory.iniciarSesion = function (nick, pass) {
            return $http({
                method: "POST",
                url: urlBase + '/usuario/login',
                data: {

                    nick: nick,
                    pass: pass
                },
                headers: {
                    'Content-Type': "application/json; charset=utf-8"
                }
            })


        };

        factory.registrarse = function (nick, nombre, apellido, email, pass) {
            return $http({
                method: "POST",
                url: urlBase + '/usuario/',
                data: {

                    nick: nick,
                    nombre:nombre,
                    apellido:apellido,
                    email:email,
                    pass: pass
                },
                headers: {
                    'Content-Type': "application/json; charset=utf-8"
                }
            })
        };

        factory.cerrarSesion = function () {
            return $http({
                method: "GET",
                url: urlBase + '/usuario/logout'
            })


        };

        return factory;
    });