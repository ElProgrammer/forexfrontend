angular
    .module('forex')
    .factory("foro.service", function ($http) {
        var urlBase = 'http://localhost:80/forEXApi/index.php/api';
        var factory = {};

        factory.getForos = function () {
        return $http.get(urlBase+'/foros');
        };

        return factory;
    });