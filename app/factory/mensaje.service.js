angular
    .module('forex')
    .factory("mensaje.service", function ($http) {
        var urlBase = 'http://localhost:80/forEXApi/index.php/api';
        var factory = {};

        factory.getMensajes = function (idTema, pagina, cantidad) {
            return $http.get(urlBase + '/temas/' + idTema + "/mensajes/" + pagina + "/" + cantidad);
        };

        factory.setMensaje = function (contenido, idTema, nick) {
            console.log(contenido);
            return $http({
                method: "POST",
                url: urlBase + '/mensajes/',
                data: {
                    contenido: contenido,
                    idTema: idTema,
                    nick: nick

                },
                headers: {
                    'Content-Type': "application/json; charset=utf-8"
                }
            })
        };

        return factory;
    });